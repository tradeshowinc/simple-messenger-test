# Simple Messenger Test

Instructions for a simple, real-time messenger.

## Instructions

Create a well-structured, tested and documented Ruby on Rails application for a simple messenger. Push your code to a public repository on GitHub.

The application should be able to do the following:

1. Receive a new message via REST API
2. Parse the message for any URLs in the message body
3. If URLs were found, parse each URL and assign an "attachment" to the message for each URL
4. Send the final message with attachments over a real-time channel (such as Faye or ActionCable) to all subscriptions

The message should have the following structure:

```
Message
  - id (integer)
  - sender_name (string)
  - body (text)
  - sent_at (datetime)
  - attachments (collection)
    - title
    - title_url
    - description
    - image_url
```

Note: If the URL is an image, we only need the image_url.